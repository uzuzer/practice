import { any } from './tasks/task-1.js'
import { arrayDiff } from './tasks/task-2.js'
import { forEachRight } from './tasks/task-3.js'
import { union } from './tasks/task-4.js'
import { createGenerator } from './tasks/task-5.js'
import { without } from './tasks/task-6.js'
import { indexOfAll } from './tasks/task-7.js'
import { membersOnActiveMeetups } from './tasks/task-8.js'
import { factory } from './tasks/task-9.js'
import { objct } from './tasks/task-10.js'
import { add } from './tasks/task-11.js'
import { camelize } from './tasks/task-12.js'
import { filterRange } from './tasks/task-13.js'
import { unique } from './tasks/task-14.js'
import { filter_list } from './tasks/task-16.js'
import { spinWords } from './tasks/task-15.js'
import { square } from './tasks/task-17.js'
import { persistence } from './tasks/task-18.js'
import { int } from './tasks/task-19.js'
import { divisors } from './tasks/task-20.js'
import { getLengthOfShortestWord } from './tasks/task-21.js'
import { isTriangle } from './tasks/task-22.js'
import { getMiddle } from './tasks/task-23.js'
import { sortOdd } from './tasks/task-24.js'
import { countRepetitions } from './tasks/task-25.js'
import { minMax } from './tasks/task-26.js'
import { reverseString } from './tasks/task-27.js'
import { uniqueInOrder } from './tasks/task-28.js'

console.group('Task 1');
console.log(any([0, 1, 2, 0], x => x >= 2));
console.log(any([0, 1, 2, 0]));
console.log(any([0, 0, 0]));
console.groupEnd();

console.group('Task 2');
console.log(arrayDiff([5, 2, 3, 4], [3, 8, 2, 1]));
console.groupEnd();

console.group('Task 3');
forEachRight([1, 2, 3, 4], val => console.log(val));
console.groupEnd();

console.group('Task 4');
console.log(union([5, 2, 3, 4], [3, 8, 2, 1]));
console.groupEnd();

console.group('Task 5');
let generator = createGenerator([1, '6', 3, 2]);
generator.next();
generator.next();
generator.next();
generator.next();
generator.next();
console.groupEnd();

console.group('Task 6');
console.log(without([5, 2, 3, 4, 3], 3, 4));
console.groupEnd();

console.group('Task 7');
console.log(indexOfAll([5, 2, 3, 4, 3], 3));
console.groupEnd();

const meetups = [
    { name: 'JavaScript', isActive: true, members: 100 },
    { name: 'Angular', isActive: true, members: 900 },
    { name: 'Node', isActive: false, members: 600 },
    { name: 'React', isActive: true, members: 500 },
];
console.group('Task 8');
console.log(membersOnActiveMeetups(meetups));
console.groupEnd();

console.group('Task 9');
const object = factory(11, 55, 'sum');
console.log(object.x, object.y, object.sum());
console.groupEnd();

console.group('Task 10');
console.log(`Name: ${objct}`);
console.log(+objct);
console.log(objct + 10);  
console.groupEnd();

console.group('Task 11');
console.log(+add(0)(1)(2)(3)(4)(5)());
console.groupEnd();

console.group('Task 12');
console.log(camelize('list-style-image'));
console.log(camelize('-webkit-transition'));
console.groupEnd();

console.group('Task 13');
console.log(filterRange([5, 3, 8, 1, 2, 7], 1, 7));
console.groupEnd();

console.group('Task 14');
console.log(unique([5, 2, 3, 4, 3]));
console.groupEnd();

console.group('Task 15');
console.log(spinWords('Hey fellow warriors', 5));
console.groupEnd();

console.group('Task 16');
console.log(filter_list([1, 2, 'aasf', '1', '123', 123]));
console.groupEnd();

console.group('Task 17');
console.log(square(91));
console.groupEnd();

console.group('Task 18');
console.log(persistence(57));
console.groupEnd();

console.group('Task 19');
console.log(int([1, 0, 0, 1]));
console.log(int([1, 1, 1, 1]));
console.groupEnd();

console.group('Task 20');
console.log(divisors(12));
console.groupEnd();

console.group('Task 21');
console.log(getLengthOfShortestWord('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'));
console.groupEnd();

console.group('Task 22');
console.log(isTriangle(1, 2, 2));
console.log(isTriangle(7, 2, 2));
console.groupEnd();

console.group('Task 23');
console.log(getMiddle('middle'));
console.log(getMiddle('testing'));
console.groupEnd();

console.group('Task 24');
console.log(sortOdd([5, 3, 8, 1, 2, 7]));
console.groupEnd();

console.group('Task 25');
console.log(countRepetitions('aabbBcde'));
console.groupEnd();

console.group('Task 26');
console.log(minMax([5, 3, 8, 1, 2, 7]));
console.groupEnd();

console.group('Task 27');
console.log(reverseString('Hey fellow warriors'));
console.groupEnd();

console.group('Task 28');
console.log(uniqueInOrder('AAAABBBCCDAABBB'));
console.log(uniqueInOrder([1, 2, 2, 3, 3]));
console.groupEnd();

