// Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет в нём элементы между a и b и отдаёт массив этих элементов.
// Например:
// let arr = [5, 3, 8, 1];
// let filtered = filterRange(arr, 1, 4);
// alert( filtered ); // 3,1

const filterRange = (arr, a, b) => {
    const min = a > b ? b : a;
    const max = a > b ? a : b;
    return arr.filter(elem => elem > min && elem < max);
}

export { filterRange };