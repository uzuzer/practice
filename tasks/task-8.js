// Функция принимает массив meetups,
// и возвращает суммарное количество человек, находящихся на активных митапах
// membersOnActiveMeetups(meetups); // 1500
// Пример: 
/*
const meetups = [
  { name: 'JavaScript', isActive: true, members: 100 },
  { name: 'Angular', isActive: true, members: 900 },
  { name: 'Node', isActive: false, members: 600 },
  { name: 'React', isActive: true, members: 500 },
];
*/
// membersOnActiveMeetups(meetups); // 1500

const membersOnActiveMeetups = (arr) => {
    return arr.filter(obj => obj.isActive && obj.members).reduce((members, objects) => members + objects.members, 0);  
}

export { membersOnActiveMeetups };