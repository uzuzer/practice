// Реализовать функцию createGenerator.
// При каждом вызове метода .next() происходит возврат следующего значения из массива
// Когда все элементы будут возвращены,
// следующие вызовы метода .next() должны возвращать строку 'Complete!'
// const generator = createGenerator([1, '6', 3, 2]);
// generator.next(); -> 1
// generator.next(); -> '6'
// generator.next(); -> 3
// generator.next(); -> 2
// generator.next(); -> 'Complete!'

function* createGenerator(arr) {
    let i = 0;
    while (i < arr.length) { 
        yield console.log(arr[i]);
        i++;
        if (i >= arr.length) {
            console.log('Complete!');
        }
    }
}

export { createGenerator };