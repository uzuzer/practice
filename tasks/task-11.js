// Каррирование. Написать ф-ию, которая может вызываться неограниченное количество раз и считать 
// общую сумму всех переданных параметров (Внимание, на node может работать некорректно. 
// При проблемах проверять работоспособность иными путями, например, через консоль браузера):
// add(4)(3)(1) => 8
// add(4)(3)(1)(2)(3) => 13
// add(1)(1)(1)(1)(1)(1)(1)(1)(1)(1) => 10

const add = (a = 0) => {
    let sum = a;
    const func = (b = 0) => {
      sum += b;
      return func;
    };
  
    func.toString = () => sum;
  
    return func;
  };

export { add };