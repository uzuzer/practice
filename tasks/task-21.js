// Дана строка. Вернуть длину самого короткого слова
// Пример:
// getLengthOfShortestWord(‘Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
// incididunt ut labore et dolore magna aliqua.’) 	// => 2

const getLengthOfShortestWord = (str) => {
    const result = str.split(' ').map(elem => elem.length);
    return Math.min(...result);
}

export { getLengthOfShortestWord };