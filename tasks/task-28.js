// Реализовать функцию uniqueInOrder, которая принимает в качестве аргумента последовательность и 
// возвращает список элементов без каких-либо элементов с одинаковым значением рядом друг с 
// другом и с сохранением исходного порядка элементов.
// Примеры:
// uniqueInOrder('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
// uniqueInOrder('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
// uniqueInOrder([1,2,2,3,3])       == [1,2,3]

const uniqueInOrder = (data) => {
    let dataArr = Array.from(data);
    return dataArr.reduce((resultArr, elem) => {
        if(resultArr[resultArr.length - 1] !== elem) {
            resultArr.push(elem);
        }
        return resultArr;
    }, []);
}

export { uniqueInOrder };