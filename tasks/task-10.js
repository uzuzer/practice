// const obj = {
//   id: 0,
//   name: 'Obj-name',
   // ...
// };
// console.log(`Name: ${obj}`); 		// Name: Obj-name
// console.log(+obj);            		// 0
// console.log(obj + 10);        		// 10

const objct = {
    id: 0,
    name: 'Obj-name',
    toString: function () {
        return this.name;
    },
    valueOf: function () {
        return this.id;
    },
};
  
export { objct };
  