// Создать функцию с именем divisors / Divisors, которая принимает целое число n> 1 и возвращает 
// массив со всеми делителями целого числа (кроме 1 и самого числа), от наименьшего до наибольшего. 
// Если число простое, вернуть строку '(integer) is prime'.
// Примеры:
// divisors(12); // should return [2,3,4,6]
// divisors(25); // should return [5]
// divisors(13); // should return "13 is prime"

const divisors = (number) => {
    let i = 2;
    let divisor = [];
    while (i < number) {
        if (number % i === 0) {
            divisor.push(i);
        }
        i++;
    }
    return divisor;
}

export { divisors };