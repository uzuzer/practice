// Реализовать функцию indexOfAll.
// Первый аргумент - массив, второй - значение
// Функция возвращает массив со всеми индексами, которые соответствуют переданному значению
// console.log(indexOfAll([1, 2, 3, 1, 2, 3], 1)); -> [0, 3]
// console.log(indexOfAll([1, 2, 3], 4)); -> []

const indexOfAll = (arr, elem) => {
    return arr.reduce((resultArr, currElem, i) => {
        if(currElem === elem) {
            resultArr.push(i);
        }
        return resultArr;
      }, []); 
}

export { indexOfAll };