// const factory = (xValue, yValue, funcSumName) => {
//   ...
//}
//const obj = factory(12, 23, 'myFunc');
//console.log(obj.x, obj.y, obj.myFunc()); // 12, 23, 35

const factory = (xValue, yValue, funcSumName) => {
    const object = {
      x: xValue,
      y: yValue,
      [funcSumName]: function () {
        return this.x + this.y;
      },
    };
    return object;
};

export { factory };  