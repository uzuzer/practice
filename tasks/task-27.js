// Написать функцию, которая принимает строковый параметр и переворачивает каждое слово в строке. 
// Все пробелы в строке следует сохранить.
// Examples
// "This is an example!" ==> "sihT si na !elpmaxe"
// "double  spaces"      ==> "elbuod  secaps"

const reverseString = (str) => {
    return str.split(' ').map(elem => {
        return elem.split('').reverse().join('');
    }).join(' ');
}

export { reverseString };