// Возвести в квадрат каждую цифру числа и соединить их.
// Например, если мы запустим 9119 через функцию, выйдет 811181, потому что 92 - это 81, а 12 - 1.
// Примечание: функция принимает целое число и возвращает целое число.

const square = (number) => {
    const resultNumber = String(number).split('').map(elem => Number(elem) ** 2).join('');
    return resultNumber;
}

export { square };