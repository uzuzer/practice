// Функция принимает 2 массива, и возвращает массив объединенных значений,
// без дублировани
// console.log(union([5, 1, 2, 3, 3], [4, 3, 2])); -> [5, 1, 2, 3, 4]
// console.log(union([5, 1, 3, 3, 4], [1, 3, 4])); -> [5, 1, 3, 4]

const union = (arr1, arr2) => {
    let resultArr = new Set(arr1.concat(arr2));
    return Array.from(resultArr);
}

export { union };